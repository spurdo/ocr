#ifndef PIXEL_OPERATIONS_H_
#define PIXEL_OPERATIONS_H_

#include <stdlib.h>
#include <SDL.h>

void save_image(SDL_Surface* image_surface);
void black_white(SDL_Surface* image_surface);
void init_sdl();
SDL_Surface* load_image(char* path);
SDL_Surface* display_image(SDL_Surface *img);
void wait_for_keypressed();
void grayscale(SDL_Surface* image, int x, int y);
void convolution(SDL_Surface* input, SDL_Surface* output, double kernel[], size_t kernelLength, int width, int height, int x, int y );
double get_average_lum_all(SDL_Surface* image, int width, int height);
double get_average_lum_grey(SDL_Surface* image, int width, int height, Uint8 averageAll);
void binarization(SDL_Surface* image,int x, int y, int averageLum);
Uint32 get_pixel(SDL_Surface *surface, unsigned x, unsigned y);
void put_pixel(SDL_Surface *surface, unsigned x, unsigned y, Uint32 pixel);
void update_surface(SDL_Surface* screen, SDL_Surface* image);

#endif
