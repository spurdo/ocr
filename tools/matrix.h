#ifndef MATRIX_H
#define MATRIX_H

typedef struct
{
    int x;
    int y;
    char *values;
} matrix;

matrix* init_matrix(int x, int y);
matrix* matrix_interpolation(matrix* m, int to_x, int to_y);
matrix* max_pooling(matrix* old);

int get_max(matrix* old, int x, int y);

int min(int a, int b);

void free_matrix( matrix *matrix);
void print_matrix( matrix* values);

char get_value( matrix* matrix, int x, int y);
void set_value( matrix* matrix, int x, int y, char value);

#endif
