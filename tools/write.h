#ifndef WRITE_H
#define WRITE_H

#include "../neural_network/Loic/Networkstruct.h"
#include "list.h"

void writetofile(list* char_list);
void write(list* char_list);
void savebias(hidden_layer* hidden, output_layer* output);


#endif
