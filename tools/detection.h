#ifndef DETECTION_H
#define DETECTION_H

#define BLACK 0
#define WHITE 255

#include "list.h"
// Tuple structure to store 2 values,
// Used to store coordinates

typedef struct Tuple Tuple;
struct Tuple{

    int x;
    int y;
};

void detectSentence(Tuple* topleft, Tuple* bottomright, SDL_Surface* image,int* nbsentences, int* nbcharacters,list* l); // <- detects sentence and returns
void detectCharacter(Tuple* topleft, Tuple* bottomright, SDL_Surface* image, int* nbcharacters,  list* characters);

void char_to_mat(list* character_list, SDL_Surface* image, int top_x, int top_y, int bot_x, int bot_y);
void drawRect(SDL_Surface* image, Tuple* topleft, Tuple* bottomright);
#endif
