#ifndef READ_H
#define READ_H

#include "../neural_network/Loic/Networkstruct.h"
#include "list.h"

//List of tuples, tuples contain (matrix, char)
list* readfromfile(const char* filename);

//List of matrices
list* load(const char* filename);
void loadbias(const char* filename, hidden_layer* hidden, output_layer* output);

#endif
