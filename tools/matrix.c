#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int x;
    int y;
    float *values;
} matrix;

 matrix* init_matrix(int x, int y) {
    int size_mat = sizeof(matrix);
    int size_tab = x * y * sizeof(float);
    //printf("mat:%d, tab:%d, %d\n", size_mat, size_tab, size_mat-size_tab);

    matrix *new_matrix = malloc(size_mat);
    new_matrix->x = x;
    new_matrix->y = y;
    new_matrix->values = malloc(size_tab);

    return new_matrix;
}

void free_matrix(matrix* matrix) {
    free(matrix->values);
    free(matrix);
}

float get_float( matrix* matrix, int x, int y) {
    return matrix->values[x * matrix->y + y];
}

void set_float( matrix* matrix, int x, int y, float value) {
    matrix->values[x * matrix->y + y] = value;
}

void print_matrix( matrix* values)
{
    int size_x = values->x;
    int size_y = values->y;
    printf("{");
    char comma = ',';
    int i = 0;
    while (i < size_x)
    {
        printf("{ ");
        int j = 0;
        while (j < size_y)
        {
            int line = i * size_y + j;
            if (j == size_y-1)
            {
                printf("%lf ", values->values[line]);
            }
            else
            {
                printf("%lf%c ", values->values[line], comma);
            }
            j++;
        }
        printf("}");
        i++;
    }
    printf("}\n");
}