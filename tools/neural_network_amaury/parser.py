def parser(filename):

    file = open(filename,'r', encoding='iso-8859-1')
    str = file.read()
    file.close()
    words = str.split('ß')
    while('' in words):
        words.remove('') 

    res = []

    for i in range(0, len(words), 2):
        word = words[i]
        mat = []
        for char in word:
            mat.append(int(char))
        res.append((mat, words[i+1]))

    return res