#ifndef NEURAL_NETWORK_H
    #define NEURAL_NETWORK_H
    double softmax(double yi, double y);
    layer* feedforward(matrix* res, matrix* hidden_weights,
                        layer* hidden_bias);
    double error(layer* output_results, layer* target);
    void derivates( layer* output_results, matrix* res, layer* target,
                matrix* hidden_weights, layer* hidden_bias,
                matrix* dhidden_x, matrix* dhidden_w, layer* dhidden_b);
    void update(matrix* dhidden_w, layer* dhidden_b,
            matrix* hidden_weights, layer* hidden_bias, double lr);
#endif