from matrix import *
from math import exp, log

def softmax(yi, y):
    return exp(yi) / y
    
def init_layer(layer_size):
    return [0] * layer_size

def feedforward(res, hidden_weights, hidden_bias):
    hidden_weights_colums = hidden_weights[1]
    hidden_weights_lines = len(hidden_weights[0]) // hidden_weights_colums

    res_colums = res[1]
    res_lines = len(res[0]) // res_colums
    hidden_results = init_layer(hidden_weights_lines)
    output_results = init_layer(hidden_weights_lines)

    # Hidden Layer
    # Pour chaque neurone
    for i in range(hidden_weights_lines):
        # pour chaque poid
        b = hidden_bias[i]
        for l in range(hidden_weights_colums):
            w = get(hidden_weights, i, l)
            sum_w = 0
            # pour chaque res
            for j in range(res_lines):
                for k in range(res_colums):
                    x = get(res, j, k)
                    sum_w += x * w
        sum_w += b
        hidden_results[i] = sum_w

    # "Output" Layer
    y = 0
    for i in range(hidden_weights_lines):
        v = hidden_results[i]
        y += exp(v)

    # pour chaque neurone
    for i in range(hidden_weights_lines):
        v = hidden_results[i]
        output_results[i] = softmax(v, y)

    return output_results

def error(output_results, target):
    output_results_size = len(output_results)

    err = 0

    sum_err = 0
    for i in range(output_results_size):
        sum_err += target[i] * log(output_results[i])
    err = (-1) * sum_err

    return err

def derivates(output_results, res, target, hidden_weights, hidden_bias):
    res_colums = res[1]
    res_lines = len(res[0]) // res_colums

    hidden_weights_colums = hidden_weights[1]
    hidden_weights_lines = len(hidden_weights[0]) // hidden_weights_colums

    dz = init_layer(len(output_results))
    dhidden_x = init_matrix(res_lines, res_colums)
    dhidden_w = init_matrix(hidden_weights_lines, hidden_weights_colums)
    dhidden_b = init_layer(len(hidden_bias))

    for k in range(len(output_results)):
        T = 0
        for j in range(len(target)):
            T += target[j]
        
        dz = output_results[k] * T - target[k]

        w = 0
        for i in range(res_lines):
            for j in range(res_colums):
                w += get(hidden_weights, i, k)
                put(dhidden_x, i, j, dz * w)

                x = 0
                for p in range(hidden_weights_lines):
                    for q in range(hidden_weights_colums):
                        if(k == q):
                            dhidden_b[q] = dz
                            if(i == p):
                                x += get(res, i, j)
                        put(dhidden_w, p, q, dz*x)

    return (dhidden_x, dhidden_w, dhidden_b)

def update(dhidden_w, dhidden_b, hidden_weights, hidden_bias, lr):
    hidden_weights_colums = hidden_weights[1]
    hidden_weights_lines = len(hidden_weights[0]) // hidden_weights_colums
    hidden_bias_size = len(hidden_bias)

    d_weights_colums = dhidden_w[1]
    d_weights_lines = len(dhidden_w[0]) // d_weights_colums

    for i in range(hidden_weights_lines):
        for j in range(hidden_weights_colums):
            dw = get(dhidden_w, i, j)
            w = get(hidden_weights, i, j)
            new_w = w - lr * dw
            put(hidden_weights, i, j, new_w)
    
    for i in range(hidden_bias_size):
        db = dhidden_b[i]
        b = hidden_bias[i]
        new_b = b - lr * db
        hidden_bias[i] = new_b