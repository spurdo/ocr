#include <stdio.h>
#include <stdlib.h>
#include "random.h"

typedef struct
{
    int x;
    int y;
    double *values;
} matrix;

typedef struct
{
    int size;
    double* values;
} layer;

layer* init_layer(int layer_size)
{
    int size_layer = sizeof(layer);
    int size_val = layer_size * sizeof(double);

    layer* new_layer = malloc(size_layer);
    new_layer->size = layer_size;
    new_layer->values = malloc(size_val);

    for (int i = 0; i < layer_size; i++)
    {
        new_layer->values[i] = 0;
    }

    return new_layer;
}

void rand_layer(layer* layer)
{
    int size = layer->size;
    for (int i = 0; i < size; i++)
    {
        layer->values[i] = rand_normal();
    }
}

int max_layer(layer* layer)
{
    int size = layer->size;
    int max_id = 0;
    for (int i = 1; i < size; i++)
    {
        if(layer->values[i] > layer->values[max_id])
        {
            max_id = i;
        }
    }
    return max_id;
}

void free_layer(layer* layer) {
    free(layer->values);
    free(layer);
}

double get(matrix* matrix, int x, int y) {
    return matrix->values[x * matrix->y + y];
}

void put( matrix* matrix, int x, int y, double value) {
    matrix->values[x * matrix->y + y] = value;
}

matrix* init_matrix(int x, int y) {
    int size_mat = sizeof(matrix);
    int size_tab = x * y * sizeof(double);

    matrix *new_matrix = malloc(size_mat);
    new_matrix->x = x;
    new_matrix->y = y;
    new_matrix->values = malloc(size_tab);

    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < y; j++)
        {
            put(new_matrix, i, j, 0);
        }
    }
    
    return new_matrix;
}

void rand_mat(matrix* matrix)
{
    int mat_colums = matrix->y;
    int mat_lines = matrix->x;
    for (int i = 0; i < mat_lines; i++)
    {
        for (int j = 0; j < mat_colums; j++)
        {
            put(matrix, i, j, rand_normal());
        }   
    }
}

void free_matrix(matrix* matrix) {
    free(matrix->values);
    free(matrix);
}

void print_layer(layer* layer)
{
    int size = layer->size;
    for (int i = 0; i < size; i++)
    {
        printf("%lf\n", layer->values[i]);
    }
    
}

void print_matrix(matrix* values)
{
    int size_x = values->x;
    int size_y = values->y;
    printf("\n");
    int i = 0;
    while (i < size_x)
    {
        int j = 0;
        while (j < size_y)
        {
            int line = i * size_y + j;
            printf("%lf ", values->values[line]);
            /*
            if ((int) values->values[line] == 1)
                printf("1");
            else
                printf(" ");*/
            j++;
        }
        printf("\n");
        i++;
    }
    printf("\n");
}