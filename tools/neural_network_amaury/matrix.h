#ifndef MATRIX_H
    #define MATRIX_H
    typedef struct
    {
        int x;
        int y;
        double *values;
    } matrix;

    typedef struct
    {
        int size;
        double* values;
    } layer;
    
    layer* init_layer(int layer_size);
    void free_layer(layer* layer);
    int max_layer(layer* layer);
    void rand_layer(layer* layer);
    matrix* init_matrix(int x, int y);
    void free_matrix( matrix *matrix);
    void rand_mat(matrix* matrix);
    double get( matrix* matrix, int x, int y);
    void put( matrix* matrix, int x, int y, double value);
    void print_layer(layer* layer);

    void print_matrix( matrix* values);
#endif