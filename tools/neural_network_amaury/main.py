from neural_network import init_layer, feedforward, error, derivates, update
from convolution import convolution
from pooling import pooling
from matrix import *
from parser import parser

def main():
    learning_rate = 0.2
    epoch = 10
    hidden_weights = init_matrix(62, 64)
    hidden_bias = init_layer(62)
    conv_weights = init_matrix(3, 3)

    rand_mat(hidden_weights)
    rand_mat(conv_weights)
    rand_layer(hidden_bias)

    inputs = parser("../ocr/training_data/gothic.txt")
    inputs = inputs[:26]

    for _ in range(epoch):
        for sample in inputs:
            input_s, good_value = sample

            l = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] #, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
            good_value = l.index(good_value)

            input_mat = init_matrix(16, 16)
    
            for i in range(len(input_s)):
                input_mat[0][i] = input_s[i]

            # res = convolution(input_s, conv)
            # res = input_s
            res = pooling(input_mat)

            output_results = feedforward(res, hidden_weights, hidden_bias)

            target = init_layer(62)
            target[good_value] = 1

            err = error(output_results, target)
            print("error -> " + str(err))

            (dhidden_x, dhidden_w, dhidden_b) = derivates(output_results, res, target, hidden_weights, hidden_bias)

            update(dhidden_w, dhidden_b, hidden_weights, hidden_bias, learning_rate)

main()