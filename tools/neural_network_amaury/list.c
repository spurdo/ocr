#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"
#include "list.h"

/*
*   Initialise a list, function returns
*   a pointer on a list, whose head and tail
*   are NULL by default.
*   List starts with a length of 0.
*/

list* init_list(){

    list*   l;
    l = malloc(sizeof(list));

    l->head = NULL;
    l->tail = NULL;
    l->length = 0;

    return l;
}

void* getat(list* l, int pos){

    node* node = NULL;

    if (pos < l->length){
        node = l->tail;
        for(int i = 0; i < pos; i++)
            node = node->next;
    }

    return node->val;
}

/*
*   To delete a list, pop() is called
*   on the list l, while its length is
*   different from 0.
*   pop will delete the last element,
*   and as it is accessible simply with just "l->head"
*   there is no need to pass through each element in
*   the list to get to the end.
*/

void delete_list(list* l){

    while(l->length > 0){
        pop(l);
    }
    free(l->head);
    free(l);
}

/*
*   To insert a matrix m into the list,
*   we first travel to the position "pos".
*   If pos is closer to head or the tail,
*   we start from the closest node to maximize efficiency.
*   We then create a new node that contains m,
*   and points to the node in pos+1 for its next value,
*   and points to the node in pos-1 for its prev value.
*   If pos is bigger than the length of the list,
*   we simply append m.
*/

void insert(list* l, int pos, void* m){

    if (pos > l->length){
        append(l, m);
    }

    else if (pos > 0){

        node*   new_node;
        node*   current_node;

        new_node = malloc(sizeof(node));
        current_node = malloc(sizeof(node));

        // pos is close to the tail.
        if (pos < (int) (l->length / 2) ){

            current_node = l->tail;

            for(int i = 0; i < pos-1; i++)
                current_node = current_node->next;
        }

        // pos is close to the head.
        else{

            current_node = l->head;

            for(int i = l->length-1; i > pos-1; i--)
                current_node = current_node->prev;
        }

        new_node->val = m;
        new_node->prev = current_node;
        new_node->next = current_node->next;

        current_node->next = new_node;

        l->length++;
    }
}

/*
*   Same principle as insert(), except instead of
*   creating a new node, we simply switch out
*   the value of the node with m.
*/

void change(list* l, int pos, void* m){

    if(pos > 0 && pos < l->length){

        node*   n;
        n = malloc(sizeof(node));

        if (pos < (int) l->length / 2){

            n = l->tail;

            for(int i = 0; i < pos; i++)
                n = n->next;
        }

        else{

            n = l->head;

            for(int i = l->length-1; i > pos; i--)
                n = n->prev;
        }

        n->val = m;
    }
}

/*
*   We set the head of the list to a new node,
*   that has a value of m.
*   The new node's next points to NULL,
*   and its prev points to the former head.
*   The former head's next points to the new node.
*   If there isn't anything in the list (length == 0),
*   we set a new node as the head and tail.
*/

void append(list* l, void* m){

    node*   n;
    n = malloc(sizeof(node));


    if(l->length > 0){

        n->prev = l->head;
        n->next = NULL;
        n->val = m;

        l->head->next = n;
        l->head = n;
    }

    else{

        n->val = m;
        n->prev = NULL;
        n->next = NULL;

        l->head = n;
        l->tail = n;
    }

    l->length++;
}

/*
*   To pop element in the list, we free the head.
*   The node before the head, becomes the new head,
*   and points to NULL.
*   If there is only one element in the list,
*   we set the head and tail to NULL, and free them.
*   If there is no element in the list, we don't do anything.
*/

void free_list(void* val)
{
    if (sizeof(val) == sizeof(tuple))
    {
        tuple* tup;
        tup = val;
        free_matrix(tup->mat);
        free(tup);
    }
    else if(sizeof(val) == sizeof(matrix))
    {
        free_matrix((matrix*) val);
    }
    else
    {
        free(val);
    }
}

void pop(list* l){
    if(l != NULL){

        if(l->length > 1){

            node*   n;

            n = l->head->prev;
            n->next = NULL;

            free_list(l->head->val);
            free(l->head);

            l->head = n;
        }

        else{
            free_list(l->head->val);
            
            l->head->next = NULL;
            l->head->prev = NULL;
            l->tail = l->head;
        }
        l->length--;
    }
}

/*
*   FOR DEBUGGING PURPOSES ONLY
*   Prints each element of the list,
*   by calling print_matrix() from "matrix.h"
*   on each node's element.
*   Prints only if list contains matrices.
*/

void lprint(list* l){

    node*   hong_kong;

    hong_kong = malloc(sizeof(hong_kong));
    hong_kong = l->tail;


    while(hong_kong != NULL){
        print_matrix(hong_kong->val);
        hong_kong = hong_kong->next;
    }
    free(hong_kong);
}