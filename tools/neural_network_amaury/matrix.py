from random import uniform

def get(mat, x, y):
    list_matrix = mat[0]
    place = x*mat[1]+y
    return list_matrix[place]

def put(mat, x, y, value):
    colum_size = mat[1]
    place = x*colum_size+y
    list_matrix = mat[0]
    list_matrix[place] = value

def init_matrix(size_line, size_colum):
    size = size_line * size_colum
    mat = ([0] * size, size_colum)
    return mat

def rand_mat(mat):
    mat_colums = mat[1]
    mat_lines = len(mat[0]) // mat_colums
    for i in range(mat_lines):
        for j in range(mat_colums):
            put(mat, i, j, uniform(0, 1))

def rand_layer(l):
    for i in range(len(l)):
        l[i] = uniform(0, 1)