#ifndef READ_H
#define READ_H

list* readfromfile(const char* filename);
list* load(const char* filename);

#endif
