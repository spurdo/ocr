#ifndef POOLING_H
    #define POOLING_H
    int get_max(matrix* old, int x, int y);
    matrix* max_pooling(matrix* old);
#endif