#ifndef LIST_H
#define LIST_H

#include "matrix.h"

typedef struct list list;
typedef struct node node;
typedef struct tuple tuple;

struct tuple{

    matrix* mat;
    char    ch;
};

struct list{

    node*   head;
    node*   tail;
    int     length;

};

struct node{

    void*   val;
    node*   next;
    node*   prev;

};


list*   init_list();

void*   getat(list* l, int pos);

void    delete_list(list* l);

void    insert(list* l, int pos, void* m);
void    change(list* l, int pos, void* m);
void    append(list* l, void* m);
void    pop(list* l);

void    lprint(list* l);

#endif
