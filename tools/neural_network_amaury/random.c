#include <stdlib.h>

double rand_normal()
{
    return (double) rand() / (double) RAND_MAX;
}

double rand_b(double a, double b)
{
    return (b-a)*rand_normal() + a;
}