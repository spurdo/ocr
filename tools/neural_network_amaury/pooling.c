#include "matrix.h"

#define TO_Y 8
#define TO_X 8

int get_max(matrix* old, int x, int y)
{
    char max = 0;
    for(int i = 0; i < 2 && max == 0; i++){
        for(int j = 0; j < 2 && max == 0; j++)
            max = get(old, x+i,y+j);
    }
    return max;
}

matrix* max_pooling(matrix* old)
{
    char val;
    matrix* new = init_matrix(TO_X,TO_Y);

    int k = 0;
    for(int i = 0; i < old->x;i+=2)
    {
        int l = 0;
        for(int j = 0; j < old->y;j+=2)
        {
            val = get_max(old, i, j);
            put(new, k, l, val);
            l++;
        }
        k++;
    }
    return new;
}