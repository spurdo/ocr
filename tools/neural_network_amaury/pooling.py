from matrix import get, put

def get_around(inp, x, y):
    # Hardcoded
    mat = ([0] * 4, 2)

    colum_size = inp[1]
    line_size = len(inp[0]) // colum_size
    k=0
    for i in range(x, x+2):
        l=0
        for j in range(y, y+2):
            value = get(inp, i, j)
            put(mat, k, l, value)
            l += 1
        k += 1
    return mat

def max_mat(mat):
    colum_size = mat[1]
    line_size = len(mat[0]) // colum_size
    max_pool = 0
    for i in range(line_size):
        for j in range(colum_size):
            value = get(mat, i, j)
            max_pool = max(value, max_pool)
    return max_pool

def pooling(inp):
    colum_size = inp[1]
    line_size = len(inp[0]) // colum_size
    # Hardcoded
    pooled = ([0] * (len(inp[0]) // 4), colum_size // 2)
    pooled_colums = pooled[1]
    pooled_lines = len(pooled[0]) // pooled_colums

    k=0
    for i in range(0, line_size, 2):
        l=0
        for j in range(0, colum_size, 2):
            mat = get_around(inp, i, j)
            max_pool = max_mat(mat)
            put(pooled, k, l, max_pool)
            l += 1
        k += 1

    return pooled