#include <math.h>
#include "matrix.h"

double softmax(double yi, double y) {
    return exp(yi) / y;
}

layer* feedforward(matrix* res, matrix* hidden_weights, layer* hidden_bias)
{
    int hidden_weights_colums = hidden_weights->y;
    int hidden_weights_lines = hidden_weights->x;

    int res_colums = res->y;
    int res_lines = res->x;
    layer* hidden_results = init_layer(hidden_weights_lines);
    layer* output_results = init_layer(hidden_weights_lines);

    // "Hidden" Layer
    // Pour chaque neurone
    for (int k = 0; k < hidden_weights_lines; k++)
    {
        double b = hidden_bias->values[k];
        double sum_w = 0;
        for (int i = 0; i < res_lines; i++)
        {
            double w = get(hidden_weights, i, k);
            for (int j = 0; j < res_colums; j++)
            {
                double x = get(res, i, j);
                sum_w += x * w;
            }
        }
        sum_w += b;
        hidden_results->values[k] = sum_w;
    }

    // "Output" Layer
    double y = 0;
    for (int i = 0; i < hidden_weights_lines; i++)
    {
        double v = hidden_results->values[i];
        y += exp(v);
    }
    
    // Pour chaque neurone
    for (int i = 0; i < hidden_weights_lines; i++)
    {
        double v = hidden_results->values[i];
        output_results->values[i] = softmax(v, y);
    }

    free_layer(hidden_results);
    
    return output_results;
}

double error(layer* output_results, layer* target)
{
    int output_results_size = output_results->size;

    double err = 0;

    double sum_err = 0;
    for (int i = 0; i < output_results_size; i++)
    {
        sum_err += target->values[i] * log(output_results->values[i]);
    }
    err = (-1) * sum_err;

    return err;
}

void derivates( layer* output_results, matrix* res, layer* target,
                matrix* hidden_weights, layer* hidden_bias,
                matrix* dhidden_x, matrix* dhidden_w, layer* dhidden_b)
{
    int res_colums = res->y;
    int res_lines = res->x;

    int hidden_weights_colums = hidden_weights->y;
    int hidden_weights_lines = hidden_weights->x;

    int output_results_size = output_results->size;
    int target_size = target->size;

    for (int k = 0; k < output_results_size; k++)
    {
        double T = 0;
        for (int j = 0; j < target_size; j++)
        {
            T += target->values[j];
        }
        
        double dz = output_results->values[k] * T - target->values[k];

        double sum_w = 0;
        for (int i = 0; i < res_lines; i++)
        {
            for (int j = 0; j < res_colums; j++)
            {
                sum_w += get(hidden_weights, i, k);
                put(dhidden_x, i, j, dz * sum_w);

                double sum_x = 0;
                for (int p = 0; p < hidden_weights_lines; p++)
                {
                    for (int q = 0; q < hidden_weights_colums; q++)
                    {
                        if (k == q)
                        {
                            dhidden_b->values[q] = dz;
                            if (i == p)
                            {
                                sum_x += get(res, i, j);
                            }
                        }
                        put(dhidden_w, p, q, dz * sum_x);
                    }   
                }   
            }   
        }   
    }   
}

void update(matrix* dhidden_w, layer* dhidden_b,
            matrix* hidden_weights, layer* hidden_bias, double lr)
{
    int hidden_weights_colums = hidden_weights->y;
    int hidden_weights_lines = hidden_weights->x;
    int hidden_bias_size = hidden_bias->size;

    for (int i = 0; i < hidden_weights_lines; i++)
    {
        for (int j = 0; j < hidden_weights_colums; j++)
        {
            double dw = get(dhidden_w, i, j);
            double w = get(hidden_weights, i, j);
            double new_w = w - lr * dw;
            put(hidden_weights, i, j, new_w);
        }
    }
    
    for (int i = 0; i < hidden_bias_size; i++)
    {
        double db = dhidden_b->values[i];
        double b = hidden_bias->values[i];
        double new_b = b - lr * db;
        hidden_bias->values[i] = new_b;
    }
    
}