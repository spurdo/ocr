#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "matrix.h"
#include "pooling.h"
#include "convolution.h"
#include "neural_network.h"
#include "list.h"
#include "read.h"

int main(int argc, char const *argv[])
{
    srand(time(NULL));

    double lr = 0.1;
    int epoch = 1;
    matrix* hidden_weights = init_matrix(62, 64);
    layer* hidden_bias = init_layer(62);
    matrix* conv_weights = init_matrix(3, 3);

    rand_mat(hidden_weights);
    rand_mat(conv_weights);
    rand_layer(hidden_bias);

    list* inputs = readfromfile("./gothic.txt");

    int in_size = inputs->length;

    for (int p = 0; p < epoch; p++)
    {
        for (int e = 0; e < in_size; e++)
        {
            tuple* tp = getat(inputs, e);

            int size_aln = 26;
            char alfanum[26] = {
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                'y', 'z' };

            // Get the index
            int good_value = 0;
            for (int i = 0; i < size_aln; i++)
            {
                if (alfanum[i] == tp->ch)
                {
                    good_value = i;
                }
            }

            matrix* res = max_pooling(tp->mat);

            layer* output_results = feedforward(res, hidden_weights,
                                                     hidden_bias);

            layer* target = init_layer(62);
            target->values[good_value] = 1;

            double err = error(output_results, target);

            int m = max_layer(output_results);
            if (m < size_aln)
            {
                char pred = alfanum[m];
                printf("real: %c --> prediction: %c\n", tp->ch, pred);
            }
            else
            {
                printf("real: %c --> lettre pas def\n", tp->ch);
            }
            
            printf("error --> %lf\n", err);

            // Back propagation
            matrix* dhidden_x = init_matrix(res->x, res->y);
            matrix* dhidden_w = init_matrix(hidden_weights->x,
                                            hidden_weights->y);
            layer* dhidden_b = init_layer(hidden_bias->size);

            derivates(output_results, res, target, hidden_weights, hidden_bias,
                        dhidden_x, dhidden_w, dhidden_b);
            
            update(dhidden_w, dhidden_b, hidden_weights, hidden_bias, lr);

            free_matrix(res);
            free_layer(output_results);
            free_layer(target);
            free_matrix(dhidden_x);
            free_matrix(dhidden_w);
            free_layer(dhidden_b);
        }
    }
    free_matrix(hidden_weights);
    free_layer(hidden_bias);
    free_matrix(conv_weights);
    delete_list(inputs);
}