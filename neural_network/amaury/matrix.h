#ifndef MATRIX_H
    #define MATRIX_H
    typedef struct
    {
        int x;
        int y;
        float *values;
    } matrix;

     matrix* init_matrix(int x, int y);
    void free_matrix( matrix *matrix);

    void print_matrix( matrix* values);

    float get_float( matrix* matrix, int x, int y);
    void set_float( matrix* matrix, int x, int y, float value);
#endif