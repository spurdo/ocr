from matrix import get, put

def get_around(input, x, y):
    # Hardcoded
    mat = ([0 * 4], 2)

    colum_size = input[1]
    line_size = len(input[0]) // colums
    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            if(i < 0 or i > line_size or j < 0 or j > colum_size):
                put(mat, i, j, 0)
            else:
                value = get(input, i, j)
                put(mat, i, j, value)
    return mat

def max_mat(mat):
    colum_size = mat[1]
    line_size = len(mat[0]) // colums
    max_pool = 0
    for i in range(line_size):
        for j in range(colum_size):
            value = get(mat, i, j)
            max_pool = max(value, max_pool)
    return max_pool

def pooling(input):
    colum_size = input[1]
    line_size = len(input[0]) // colums
    # Hardcoded
    pooled = ([0 * len(input[0] // 4)], colum_size // 2)

    for i in range(line_size):
        for j in range(colum_size):
            mat = get_around(input, i, j)
            max_pool = max_mat(mat)
            put(pooled, i, j, max_pool)

    return pooled