from matrix import get, put

def get_around(input, x, y):
    # Hardcoded
    mat = ([0 * 9], 3)
    colum_size = input[1]
    line_size = len(input[0]) // colums
    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            if(i < 0 or i > line_size or j < 0 or j > colum_size):
                put(mat, i, j, 0)
            else:
                value = get(input, i, j)
                put(mat, i, j, value)
    return mat

def hadamard_prod(conv, mat):
    colum_size = conv[1]
    line_size = len(conv[0]) // colums
    had_mat = 0

    for i in range(line_size):
        for j in range(colum_size):
            conv_value = get(conv, i, j)
            mat_value = get(mat, i, j)
            value = conv_value * mat_value
            had_mat += value
    
    return had_mat
            

def convolution(input, conv):
    """
    conv = ([w0, w1, w2,
            w3, w4, w5,
            w6, w7, w8], 3)
    """

    colum_size = input[1]
    line_size = len(input[0]) // colums
    
    convolutioned = ([0 * len(input[0])], colum_size)

    for i in range(line_size):
        for j in range(colum_size):
            mat = get_around(input, i, j)
            had_mat = hadamard_prod(conv, mat)
            put(convolutioned, i, j, had_mat)

    return convolutioned

def back_convolution(err_convolutioned, input, conv, lr):
    colum_size = err_convolutioned[1]
    line_size = len(err_convolutioned[0]) // colums

    """
    colum_size_conv = err_convolutioned[1]
    line_size_conv = len(err_convolutioned[0]) // colums

    for k in range(line_size_conv):
        
        for l in range(colum_size_conv):
            

            for i in range(min_i, line_size):
                for j in range(0+l, colum_size-l)
    """
    
    # Hardcoded

    derr_w0 = 0
    for i in range(1, line_size):
        for j in range(1, colum_size):
            new_w0 += get(input, i-1, j-1) * get(err_convolutioned, i, j)
    
    derr_w1 = 0
    for i in range(1, line_size):
        for j in range(colum_size):
            new_w1 += get(input, i-1, j) * get(err_convolutioned, i, j)

    derr_w2 = 0
    for i in range(1, line_size):
        for j in range(colum_size-1):
            new_w2 += get(input, i-1, j+1) * get(err_convolutioned, i, j)

    derr_w3 = 0
    for i in range(line_size):
        for j in range(1, colum_size):
            new_w3 += get(input, i, j-1) * get(err_convolutioned, i, j)

    derr_w4 = 0
    for i in range(line_size):
        for j in range(colum_size):
            new_w4 += get(input, i, j) * get(err_convolutioned, i, j)
    
    derr_w5 = 0
    for i in range(line_size):
        for j in range(colum_size-1):
            new_w5 += get(input, i, j+1) * get(err_convolutioned, i, j)
    
    derr_w6 = 0
    for i in range(line_size-1):
        for j in range(1, colum_size):
            new_w6 += get(input, i+1, j-1) * get(err_convolutioned, i, j)

    derr_w7 = 0
    for i in range(line_size-1):
        for j in range(colum_size):
            new_w7 += get(input, i+1, j) * get(err_convolutioned, i, j)

    derr_w8 = 0
    for i in range(line_size-1):
        for j in range(colum_size-1):
            new_w8 += get(input, i+1, j+1) * get(err_convolutioned, i, j)

    new_w0 = get(conv, 0, 0) - lr*derr_w0
    new_w1 = get(conv, 0, 1) - lr*derr_w1
    new_w2 = get(conv, 0, 2) - lr*derr_w2
    new_w3 = get(conv, 1, 0) - lr*derr_w3
    new_w4 = get(conv, 1, 1) - lr*derr_w4
    new_w5 = get(conv, 1, 2) - lr*derr_w5
    new_w6 = get(conv, 2, 0) - lr*derr_w6
    new_w7 = get(conv, 2, 1) - lr*derr_w7
    new_w7 = get(conv, 2, 2) - lr*derr_w8

    put(conv, 0, 0, new_w0)
    put(conv, 0, 1, new_w1)
    put(conv, 0, 2, new_w2)
    put(conv, 1, 0, new_w3)
    put(conv, 1, 1, new_w4)
    put(conv, 1, 2, new_w5)
    put(conv, 2, 0, new_w6)
    put(conv, 2, 1, new_w7)
    put(conv, 2, 2, new_w8)


    

    
