from matrix import *
from math import exp, log

def sigmoid(x):
    1 / (1 + exp(-x))

def softmax(yi, y):
    return exp(yi) / y
    
def init_layer(layer_size):
    return [0 * layer_size]

def feedforward(input, hidden_weights, hidden_bias, possibilities):
    possibilities_size = len(possibilities)
    hidden_weights_size = len(hidden_weights)
    input_colums = input[1]
    input_lines = len(input[0]) // input_colums
    hidden_result = init_layer(hidden_weights_size)
    output_results = init_layer(hidden_weights_size)

    # Hidden Layer
    # Pour chaque neurone
    for i in range(hidden_weights_size):
        w = hidden_weights[i]
        b = hidden_bias
        sum_w = 0
        # pour chaque input
        for j in range(input_lines):
            for k in range(input_colums):
                x = get(input, j, k)
                sum_w += x * w
        sum_w += b
        hidden_result[i] = sigmoid(sum_w)

    # Output Layer
    y = 0
    for i in range(hidden_weights_size):
        y += exp(hidden_weights[i])

    # pour chaque neurone
    for i in range(hidden_weights_size):
        output_results[i] = softmax(hidden_result[i], y)

    return output_results

def error(predictions, good_value):
    sum_err = 0
    predictions_size = len(predictions)
    for i in range(1, predictions_size):
        sum_err += good_value * log(predictions[i]) + (1 - good_value) * log(1 - predictions[i])
    sum_err = sum_err / predictions_size
    sum_err *= -1

    return sum_err
