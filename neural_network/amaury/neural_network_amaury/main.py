from neural_network import init_layer, feedforward
from convolution import convolution
from pooling import pooling
from matrix import *

def main():
    hidden_weights = init_layer(62)
    hidden_bias = 0
    conv_weights = init_matrix(3, 3)

    inputs = parser()

    
    for sample in inputs:
        input_s, good_value = sample

        # res = convolution(input_s, conv)
        res = input_s
        # res = pooling(res)

        output_results = feedforward(input_s, hidden_weights, hidden_bias)

        err = error(output_results, good_value)


    