def get(mat, x, y):
    list_matrix = mat[0]
    place = x*mat[1]+y
    return list_matrix

def put(mat, x, y, value):
    colum_size = mat[1]
    place = x*colum_size+y
    list_matrix = mat[0]
    list_matrix[place] = value

def init_matrix(size_line, size_colum):
    size = size_line * size_colum
    mat = ([0 * size], size_colum)
    return mat