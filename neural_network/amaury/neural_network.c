#include <math.h>
#include "matrix.h"
#include "matrix_operations.h"

float sigmoid(float x)
{
    return 1/(1 + exp(-x));
}

float sigmoid_derivate(float x)
{
    return x*(1-x);
}

matrix* sigmoid_mat(matrix* mat) {
    matrix* out = init_matrix(mat->x, mat->y);
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            float sig_value = sigmoid(get_float(mat, i, j));
            set_float(out, i, j, sig_value);
        }
    }
    return out;
}

matrix* sigmoid_derivate_mat(matrix* mat) {
    matrix* out = init_matrix(mat->x, mat->y);
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            float sig_value = sigmoid_derivate(get_float(mat, i, j));
            set_float(out, i, j, sig_value);
        }
    }
    return out;
}

matrix* forward_propagation(matrix* input, matrix* hidden_weights, matrix* hidden_bias) {
    matrix* hidden_layer_activation = dot(input, hidden_weights);
    sum(hidden_layer_activation, hidden_bias);
    matrix* hidden_layer_output = sigmoid_mat(hidden_layer_activation);
    free_matrix(hidden_layer_activation);
    return hidden_layer_output;
}

matrix* backprop_d_predicted_output(matrix* desired_output, matrix* predicted_output) {
    matrix* op_mat = transpose_matrix(desired_output);
    matrix* error = sub(op_mat, predicted_output);
    matrix* op_mat1 = sigmoid_derivate_mat(predicted_output);
	matrix* d_predicted_output = product(error, op_mat1);
    free_matrix(error);
    free_matrix(op_mat);
    free_matrix(op_mat1);
    return d_predicted_output;
}

matrix* backprop_d_hidden_layer(matrix* output_weights, matrix* d_predicted_output, matrix* hidden_layer_output) {
    matrix* op_mat2 = transpose_matrix(output_weights);
    matrix* error_hidden_layer = dot(d_predicted_output, op_mat2);
    matrix* op_mat3 = sigmoid_derivate_mat(hidden_layer_output);
    matrix* d_hidden_layer = product(error_hidden_layer, op_mat3);
    free_matrix(op_mat2);
    free_matrix(error_hidden_layer);
    free_matrix(op_mat3);
    return d_hidden_layer;
}

void update_weights(matrix* hidden_layer_output, matrix* d_predicted_output, matrix* output_weights, float lr) {
    matrix* op_mat4 = transpose_matrix(hidden_layer_output);
    matrix* op_mat5 = dot(op_mat4, d_predicted_output);
    matrix* op_mat6 = scal_product(op_mat5, lr);
    sum(output_weights, op_mat6);
    free_matrix(op_mat4);
    free_matrix(op_mat5);
    free_matrix(op_mat6);
}

void update_bias(matrix* d_predicted_output, matrix* output_bias, float lr) {
    matrix* op_mat7 = s(d_predicted_output);
    matrix* op_mat8 = scal_product(op_mat7, lr);
    sum(output_bias, op_mat8);
    free_matrix(op_mat7);
    free_matrix(op_mat8);
}

void backward_propagation(matrix* desired_output, matrix* predicted_output, matrix* output_weights, matrix* hidden_layer_output, matrix* output_bias, matrix* hidden_weights, matrix* input, matrix* hidden_bias, float lr) {
    matrix* d_predicted_output = backprop_d_predicted_output(desired_output, predicted_output);

    matrix* d_hidden_layer = backprop_d_hidden_layer(output_weights, d_predicted_output, hidden_layer_output);

    /*
    * Updates of Bias and Weights
    */

    update_weights(hidden_layer_output, d_predicted_output, output_weights, lr);

    update_bias(d_predicted_output, output_bias, lr);

    update_weights(input, d_hidden_layer, hidden_weights, lr);

    update_bias(d_hidden_layer, hidden_bias, lr);

    free_matrix(d_predicted_output);
    free_matrix(d_hidden_layer);
}