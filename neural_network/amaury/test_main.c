#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "random.h"
#include "matrix.h"
#include "neural_network.h"
#include "matrix_operations.h"

int main(int argc, char const *argv[])
{
    srand(time(NULL));

    /*
     * Inputs and Variables
     */

    int epochs = 10000;
    float lr = 0.1;
    int input_layer = 2;
    int hidden_layer = 2;
    int output_layer = 1;

    matrix* input = init_matrix(4, 2);
    float input_t[8] = {0, 0, 1, 0, 0, 1, 1, 1 };
    fill_arr_matrix(input, input_t);

    matrix* desired_output = init_matrix(1, 4);
    float desired_output_t[4] = {0, 1, 1, 0};
    fill_arr_matrix(desired_output, desired_output_t);

    /*
     * Initialization
     */

    matrix* hidden_weights = init_matrix(input_layer, hidden_layer);
    fill_randbt_matrix(hidden_weights, 0.5, 1);
    matrix* hidden_bias = init_matrix(1, hidden_layer);
    fill_rand_matrix(hidden_bias);

    matrix* output_weights = init_matrix(hidden_layer, output_layer);
    fill_randbt_matrix(output_weights, 0.5, 1);
    matrix* output_bias = init_matrix(1, output_layer);
    fill_rand_matrix(output_bias);

    
    printf("Before training\n");
    printf("hidden_weights :\n");
    print_matrix(hidden_weights);
    printf("hidden_bias :\n");
    print_matrix(hidden_bias);
    printf("output_weights :\n");
    print_matrix(output_weights);
    printf("output_bias :\n");
    print_matrix(output_bias);
    
    for (int i = 0; i < epochs; i++)
    {
        /*
        * Forward Propagation
        */
        matrix* hidden_layer_output = forward_propagation(input, hidden_weights, hidden_bias);

        matrix* predicted_output = forward_propagation(hidden_layer_output, output_weights, output_bias);

        /*
        * Backward Propagation
        */
        backward_propagation(desired_output, predicted_output, output_weights, hidden_layer_output, output_bias, hidden_weights, input, hidden_bias, lr);

        if (i == epochs - 1)
        {
            printf("\nAfter training\n");
            printf("hidden_weights :\n");
            print_matrix(hidden_weights);
            printf("hidden_bias :\n");
            print_matrix(hidden_bias);
            printf("output_weights :\n");
            print_matrix(output_weights);
            printf("output_bias :\n");
            print_matrix(output_bias);
            printf("Final output:\n");
            print_matrix(predicted_output);

            free_matrix(hidden_layer_output);
            free_matrix(predicted_output);
        }
        
    }

    free_matrix(input);
    free_matrix(desired_output);
    free_matrix(hidden_weights);
    free_matrix(hidden_bias);
    free_matrix(output_weights);
    free_matrix(output_bias);
    
    return 0;
}