def parser(filename):

    file = open(filename,'r',encoding='ASCII')
    str = file.read()
    file.close()
    words = str.strip(' ')

    res = []

    for i in range(0,71,2):
        mat = []
        for bin in words[i]:
            mat.append(bin-0x30)
        res.append(mat,words[i+1])

    print(res)
    return res

parser("../../training_data/gothic.txt")
