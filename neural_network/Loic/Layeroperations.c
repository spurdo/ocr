#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Networkstruct.h"
#include "Neuronoperations.h"

float* layer_Hiddenforward(hidden_layer *hidden, float inputs[], float output[])
{
    for (int i = 0; i < hidden->nb_hidden_neurons; i++)
    {
        output[i] = hidden_feedforward(&hidden->neurons[i], inputs);
    }

    return output;
}

float* layer_Outputforward(output_layer *output, float inputs[], float out[])
{
    for (int i = 0; i < output->nb_output_neurons; i++)
    {       
        out[i] = output_feedforward(&output->neurons[i], inputs);
    }
    return out;
}

void layer_UpdateHidden(hidden_layer *layer, float inputs[], float backpropout[], float error)
{
    for (int i = 0; i < layer->nb_hidden_neurons; i++)
    {
        UpdateHidden(&layer->neurons[i], inputs, backpropout[i], error);
    }
}

float** layer_UpdateOutput(output_layer *layer, float inputs[], float error[], float l[], float* li[])
{
    for (int i = 0; i < layer->nb_output_neurons; i++)
    {
        li[i] = UpdateOutput(&layer->neurons[i], inputs, error[i], l);
    }

    return li;
}

void backpropagation(output_layer *output, hidden_layer *hidden, float inputs[], float expected, float lr, float res[])
{
    float input[hidden->nb_hidden_neurons];
    for (int i = 0; i < hidden->nb_hidden_neurons; i++)
    {
        input[i] = hidden_feedforward(&hidden->neurons[i], inputs);
    }


    for (int i = 0; i < output->nb_output_neurons; i ++)
    {
        //calculating the error and multiplying it with the learing rate
        float err = expected - output_feedforward(&output->neurons[i], input);
        float error_lr = -2 * (err) * lr;

        //creating the list that will contain the output of the UpateOutput function
        float l[hidden->nb_hidden_neurons];

        //Updating the parameters of the neurons
        layer_UpdateHidden(hidden, inputs, UpdateOutput(&output->neurons[i], input, error_lr, l), error_lr);
        //returning the value of the feedforward of the ouput neuron
        res[i] = output_feedforward(&output->neurons[i], input);
    }
}

void train(output_layer *output, hidden_layer *hidden, float inputs[NB_SET][INPUT_SIZE + 1], int epochs, float lr)
{
    printf("\nlast feedforward result: \n");

    for(int i = 0; i < epochs; i++)
    {
        for (int j = 0; j < NB_SET; j++)
        {
            float input[INPUT_SIZE + 1];
            for (size_t k = 0; k < INPUT_SIZE + 1; k++)
            {
                input[k] = inputs[j][k];
            }
                
            float res[OUTPUT_SIZE];

            backpropagation(output, hidden, input, input[INPUT_SIZE], lr, res);

            if(i == epochs -1)
            {
                for (int i = 0; i < OUTPUT_SIZE; i++)
                {
                    printf("%c -> %c\n", (char) input[INPUT_SIZE], (char) ((int) roundf(res[i])));
                }
            }
        }

    }
}