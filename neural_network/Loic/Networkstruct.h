#ifndef Networkstruct_H
#define Networkstruct_H

#define NB_SET 62
#define INPUT_SIZE 256
#define HIDDEN_SIZE 36
#define OUTPUT_SIZE 1

//definition of a neuron
typedef struct
{
    int nb_hidden_weights;  //number of weights in the neuron
    float hidden_bias;  //bias value of the neuron
    float hidden_weights[INPUT_SIZE];  //list containing the weights of the neuron
}hidden_neuron;

//definition of a neuron
typedef struct
{
    int nb_output_weights;  //number of weigths in the neuron
    float output_bias;  //bias value of the neuron
    float output_weights[HIDDEN_SIZE];  //list containing the weigths of the neuron
}output_neuron;

//definition of a layer
typedef struct
{
    int nb_hidden_neurons; //number of neurons in the layer
    hidden_neuron neurons[HIDDEN_SIZE];  //list containing the neurons of the layer
}hidden_layer;

//definition of a layer
typedef struct
{
    int nb_output_neurons; //number of neurons in the layer
    output_neuron neurons[OUTPUT_SIZE];  //list containing the neurons of the layer
}output_layer;

/* function initializing a hidden_neuron */
hidden_neuron init_hidden_neuron(int nb_weights, float bias, float weights[]);

/* function initializing a output_neuron */
output_neuron init_output_neuron(int nb_weights, float bias, float weights[]);

/* function initializing a hidden_layer */
hidden_layer init_hidden_layer(int nb_neurons, hidden_neuron neurons[]);

/* function initializing a output_layer */
output_layer init_output_layer(int nb_neurons, output_neuron neurons[]);

#endif