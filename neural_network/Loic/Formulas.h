#ifndef Formulas_H
#define Formulas_H

/* reLu function */
float relu(float x); 

/* derivative of the reLu function */
float relu_derivative(float x);

/* sigmoid function */
float sigmoid(float x);

/* derivative of the sigmoid function */
float sigmoid_derivative(float x);

#endif