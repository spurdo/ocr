#include "Networkstruct.h"

 hidden_neuron init_hidden_neuron(int nb_weights, float bias, float weights[])
 {
     hidden_neuron neuron;
     neuron.nb_hidden_weights = nb_weights;
     neuron.hidden_bias = bias;
     for(int i = 0; i < nb_weights; i++)
     {
         neuron.hidden_weights[i] = weights[i];
     }

     return neuron;
 }

 output_neuron init_output_neuron(int nb_weights, float bias, float weights[])
 {
     output_neuron neuron;
     neuron.nb_output_weights = nb_weights;
     neuron.output_bias = bias;
     for(int i = 0; i < nb_weights; i++)
     {
         neuron.output_weights[i] = weights[i];
     }

     return neuron;
 }

 hidden_layer init_hidden_layer(int nb_neurons, hidden_neuron neurons[])
 {
     hidden_layer layer;
     layer.nb_hidden_neurons = nb_neurons;
     for(int i = 0; i < nb_neurons; i++)
     {
         layer.neurons[i] = neurons[i];
     }

     return layer;
 }

 output_layer init_output_layer(int nb_neurons, output_neuron neurons[])
 {
     output_layer layer;
     layer.nb_output_neurons = nb_neurons;
     for(int i = 0; i < nb_neurons; i++)
     {
         layer.neurons[i] = neurons[i];
     }

     return layer;
 }