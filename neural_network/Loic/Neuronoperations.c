#include "Formulas.h"
#include "Networkstruct.h"

float hidden_feedforward(hidden_neuron *neuron, float inputs[])
{
    float val = neuron->hidden_bias;

    for(int i = 0; i < neuron->nb_hidden_weights; i++)
    {
        val += neuron->hidden_weights[i] * inputs[i];
    }
    return relu(val);
}

float output_feedforward(output_neuron *neuron, float inputs[])
{
    float val = neuron->output_bias;

    for(int i = 0; i < neuron->nb_output_weights; i++)
    {
        val += neuron->output_weights[i] * inputs[i];
    }
    return relu(val);
}

void UpdateHidden(hidden_neuron *h, float inputs[], float backpropoutput, float error)
{
    float forward_derivative = relu_derivative(hidden_feedforward(h, inputs));

    float weightchanges[h->nb_hidden_weights];

    for(int i = 0; i < h->nb_hidden_weights; i++)
    {
        weightchanges[i] = inputs[i] * forward_derivative;
        h->hidden_weights[i] -= error * backpropoutput * weightchanges[i];
    }

    h->hidden_bias-= error * backpropoutput * forward_derivative;
}

float* UpdateOutput(output_neuron *o1, float inputs[], float error, float l[])
{
    float forward_derivative = relu_derivative(output_feedforward(o1, inputs));

    float weightchanges[o1->nb_output_weights];

    for(int i = 0; i < o1->nb_output_weights; i++)
    {
        l[i] = o1->output_weights[i] * forward_derivative;
        weightchanges[i] = inputs[i] * forward_derivative;
        o1->output_weights[i] -= error * weightchanges[i];
    }
    o1->output_bias -= error * forward_derivative;

    return l;
}