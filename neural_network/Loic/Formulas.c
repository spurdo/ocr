#include <math.h>

float relu(float x)
{
    return (x > 0 ? x : 0.001*x);
}

float relu_derivative(float x)
{
    return (x > 0 ? 1 : 0.001);
}

float sigmoid(float x)
{
    return 1/(1 + exp(-x));   
}

float sigmoid_derivative(float x)
{
    return x*(1-x);
}