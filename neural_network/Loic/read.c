#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include "../../tools/matrix.h"
#include "../../tools/list.h"
#include "../../tools/read.h"

/*
*   This function takes a filename as the only argument.
*   The corresponding file will be read, and will be parsed.
*   The return value is a list of tuples. Each tuple contains
*   a matrix and a character indicating which letter is contained
*   within the matrix.
*   Used for training only.
*/

list* readfromfile(const char* filename){

    int     end;
    size_t  succ;
    char    alphanum;
    char    current_char[1];

    matrix* mat;
    list*   total;
    tuple*  value;
    FILE*   fp;

    total = init_list();

    fp = fopen(filename,"r");
    fseek(fp, 0, SEEK_SET);

    succ = 0;
    end = 0;

    while(end < 62){

        mat = init_matrix(16,16);
        value = malloc(sizeof(tuple*));

        for (char i = 0; i < 16; i++){
            for (char j = 0; j < 16; j++){
                succ += fread(current_char,1,1,fp);
                set_value(mat, i, j, current_char[0] - 0x30);
            }
        }

        fseek(fp,1,SEEK_CUR);

        succ += fread(current_char,1,1,fp);
        alphanum = current_char[0];

        fseek(fp,1,SEEK_CUR);

        value->mat = mat;
        value->ch = alphanum;

        append(total, value);

        end++;
    }

    printf("\nSuccessfully read %ld bytes from %s\n",succ,filename);
    printf("%d letters in list.",total->length);
    fclose(fp);

    return total;
}

/*
*   Like readfromfile, but not for training.
*/

list* load(const char* filename){

    int     end;
    size_t  succ;
    char    current_char[1];

    matrix* mat;
    list*   total;
    FILE*   fp;

    total = init_list();

    fp = fopen(filename,"r");
    fseek(fp, 0, SEEK_SET);

    succ = 0;
    end = 0;

    while(end != EOF){

        mat = init_matrix(16,16);

        for (char i = 0; i < 16; i++){
            for (char j = 0; j < 16; j++){
                succ += fread(current_char,1,1,fp);
                set_value(mat, i, j, current_char[0] - 0x30);
            }
        }
        append(total, mat);
        end = getc(fp);
        fseek(fp,-1,SEEK_CUR);
    }

    printf("\nSuccessfully read %ld bytes from %s\n",succ,filename);
    printf("%d letters in list.\n",total->length);
    fclose(fp);

    return total;
}

void loadbias(const char* filename, hidden_layer* hidden, output_layer* output){

    float   val;
    FILE*   fp;

    int bytes = 0;

    fp = fopen(filename,"r");

    if (fp == NULL)
        errx(-42,"File not found!");

    for(int i = 0; i < hidden->nb_hidden_neurons; i++){
        for(int j = 0; j < hidden->neurons[i].nb_hidden_weights; j++){
            bytes += fscanf(fp,"%f",&val);
            hidden->neurons[i].hidden_weights[j] = val;
        }
        bytes += fscanf(fp,"%f",&val);
        hidden->neurons[i].hidden_bias = val;
    }

    for(int i = 0; i < output->nb_output_neurons;i++){
        for(int j = 0; j < output->neurons[i].nb_output_weights;j++){
            bytes += fscanf(fp,"%f",&val);
            output->neurons[i].output_weights[j] = val;
        }
        bytes += fscanf(fp,"%f",&val);
        output->neurons[i].output_bias = val;
    }

    printf("%d bytes read in %s",bytes, filename);
}
