#ifndef Neuronoperations_H
#define Neuronoperations_H

/* function computing the output of a hidden_neuron */
float hidden_feedforward(hidden_neuron *neuron, float inputs[]);

/* function computing the output of a output_neuron */
float output_feedforward(output_neuron *neuron, float inputs[]);

/* function updating a hidden_neurons weigths and bias */
void UpdateHidden(hidden_neuron *h, float inputs[], float backpropoutput, float error);

/* function updating a output_neurons weigths and bias */
float* UpdateOutput(output_neuron *o1, float inputs[], float error, float l[]);

#endif