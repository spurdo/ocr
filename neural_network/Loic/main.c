#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <err.h>

#include "../../tools/write.h"
#include "../../tools/matrix.h"
#include "../../tools/list.h"
#include "../../tools/read.h"
#include "Networkstruct.h"
#include "Layeroperations.h"

#define MAX_TRAIN 500

void training(list* l, char* file,output_layer output, hidden_layer hidden){

    node*   n;
    tuple*  t;
    int     count = 0;

    l = readfromfile(file);
    n = l->tail;

    float inputs[l->length][INPUT_SIZE+1];

    for (int i = 0; i < NB_SET; i++, n = n->next){
        t = n->val;

        for (int x = 0; x < t->mat->x; x++){
            for (int y = 0; y < t->mat->y; y++){
                inputs[i][count] = (float) get_value(t->mat, x, y);
                count++;
            }
        }
        count=0;
        inputs[i][INPUT_SIZE] = t->ch;
    }

    int epochs = 1000;
    float lr = 0.001;

    for(int i = 0; i < MAX_TRAIN; i++){
        printf("\nLoading bias...\nTraining %d out of %d\n",i+1,MAX_TRAIN);
        loadbias("datasave.txt",&hidden, &output);
        train(&output, &hidden, inputs, epochs, lr);
        savebias(&hidden, &output);
    }
}

void recognition(list* l, char* file, output_layer output, hidden_layer hidden){

    node*       n;
    matrix*     mat;
    int         count = 0;

    l = load(file);
    n = l->tail;

    float inputs[l->length][INPUT_SIZE+1];

    for (int i = 0; i < l->length; i++, n = n->next){
        mat = n->val;
        for (int x = 0; x < mat->x; x++){
            for(int y = 0; y < mat->y; y++){
                inputs[i][count] = (float) get_value(mat,x,y);
                count++;
            }
        }
        count = 0;
    }

    loadbias("datasave.txt", &hidden, &output);

    printf("\n\nOutput: ");

    for(int i = 0; i < l->length; i++)
    {
        float hidden_forward[HIDDEN_SIZE];
        float out[1];
        layer_Outputforward(&output, layer_Hiddenforward(&hidden, inputs[i], hidden_forward), out);
        printf("%c", (char) ((int) roundf(out[0])));
    }

    printf("\n");

}


int main(int argc, char* argv[])
{

    if (argc < 2 || argv[1] == NULL)
        errx(1,"No file specified");

    /* CREATING NEURONS AND LAYERS */
    float weights[INPUT_SIZE];
    for (int i = 0; i < INPUT_SIZE; i++)
        weights[i] = 0.5;

    hidden_neuron hiddenlist[HIDDEN_SIZE];
    for (int i = 0; i < HIDDEN_SIZE; i++)
        hiddenlist[i] = init_hidden_neuron(INPUT_SIZE, 0.5, weights);

    output_neuron outputlist[OUTPUT_SIZE];
    for (int i = 0; i < OUTPUT_SIZE; i++)
        outputlist[i] = init_output_neuron(HIDDEN_SIZE, 0.5, weights);

    hidden_layer hidden = init_hidden_layer(HIDDEN_SIZE, hiddenlist);
    output_layer output = init_output_layer(OUTPUT_SIZE, outputlist);

    list*   l = NULL;

    if (*argv[2] == '0')
        training(l, argv[1], output, hidden);

    else if (*argv[2] == '1')
        recognition(l, argv[1], output, hidden);

    else if (argv[2] == NULL || (*argv[2] != '1' && *argv[2] != '0'))
        errx(42,"Invalid parameter.\n0 -> training\n1 -> recognition");

    delete_list(l);

    return 0;
}
