#include <stdlib.h>

float rand_normal()
{
    return (float) rand() / (float) RAND_MAX;
}

float rand_b(float a, float b)
{
    return (b-a)*rand_normal() + a;
}