#ifndef Layeroperations_H
#define Layeroperations_H

/* Function doing a feedforward for every hidden_neuron in the hidden_layer */
float* layer_Hiddenforward(hidden_layer *hidden, float inputs[], float output[]);

/* Function doing a feedforward for every output_neuron in the output_layer */
float* layer_Outputforward(output_layer *output, float inputs[], float out[]);

/* Function updating the values of the weigths and bias of the hidden_neurons of the hidden_layer */
void layer_UpdateHidden(hidden_layer *layer, float inputs[], float backpropout[], float error);

/* Function updating the values of the weigths and bias of the output_neurons of the output_layer */
float** layer_UpdateOutput(output_layer *layer, float inputs[], float error[], float l[], float* li[]);

/* Function computing the changes that will be applied to the hidden_neurons and the output_neurons */
void backpropagation(output_layer *output, hidden_layer *hidden, float inputs[], float expected, float lr, float res[]);

/* Function training the network */
void train(output_layer *output, hidden_layer *hidden, float inputs[NB_SET][INPUT_SIZE+1], int epochs, float lr);

#endif
