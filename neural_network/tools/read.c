#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include "../../tools/matrix.h"
#include "../../tools/list.h"
#include "../../tools/read.h"

/*
*   This function takes a filename as the only argument.
*   The corresponding file will be read, and will be parsed.
*   The return value is a list of tuples. Each tuple contains
*   a matrix and a character indicating which letter is contained
*   within the matrix.
*   Used for training only.
*/

list* readfromfile(const char* filename){

    int     end;
    size_t  succ;
    char    alphanum;
    char    current_char[1];

    matrix* mat;
    list*   total;
    tuple*  value;
    FILE*   fp;

    total = init_list();

    fp = fopen(filename,"r");
    fseek(fp, 0, SEEK_SET);

    succ = 0;
    end = 0;

    while(end < 36){

        mat = init_matrix(16,16);
        value = malloc(sizeof(tuple*));

        for (char i = 0; i < 16; i++){
            for (char j = 0; j < 16; j++){
                succ += fread(current_char,1,1,fp);
                set_value(mat, i, j, current_char[0] - 0x30);
            }
        }

        fseek(fp,1,SEEK_CUR);

        succ += fread(current_char,1,1,fp);
        alphanum = current_char[0];

        fseek(fp,1,SEEK_CUR);

        value->mat = mat;
        value->ch = alphanum;

        append(total, value);

        end++;
    }

    printf("\nRead %ld bytes from %s",succ,filename);
    fclose(fp);

    return total;
}
