#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include "../../tools/read.h"
#include "../../tools/write.h"
#include "../../tools/matrix.h"

int main(int argc, char* argv[]){

    list*   training;
    node*   curr;
    tuple*  t;

    training = readfromfile(argv[1]);

    if (argc < 2)
        errx(1,"Not enough arguments. Missing filename.");
    printf("\nSuccessfully read %d elements from file: %s.\n\n",training->length,argv[1]);

    curr = training->tail;

    // Debugger: Prints every element in the list.
    for (; curr != NULL; curr = curr->next){
        t = curr->val;
        print_matrix(t->mat);
        printf("\nCharacter: %c\n",t->ch);
    }

    return 0;
}
