#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../../tools/matrix.h"

#define TO_Y 8
#define TO_X 8

/*
*   Matrix contents set to "char",
*   floats not needed to set binary
*   values, waste of memory.
*/


matrix* init_matrix(int x, int y) {
    int size_mat = sizeof(matrix);
    int size_tab = x * y * sizeof(char);

    matrix *new_matrix = malloc(size_mat);
    new_matrix->x = x;
    new_matrix->y = y;
    new_matrix->values = malloc(size_tab);

    return new_matrix;
}

int get_max(matrix* old, int x, int y){

    int max = 0;
    for(int i = 0; i < 2 && max == 0; i++){
        for(int j = 0; j < 2 && max == 0; j++)
            max = get_value(old, x+i,y+j);
    }
    return max;
}

matrix* max_pooling(matrix* old){

    int val;
    matrix* new = init_matrix(TO_X,TO_Y);

    for(int i = 0; i < old->x-1;i+=2){
        for(int j = 0; j < old->y-1;j+=2){
            val = get_max(old, i, j);
            set_value(new, i/2, j/2, val);
        }
    }
    return new;
}


matrix* matrix_interpolation(matrix* old, int to_x, int to_y){

    matrix*         new;
    char            value;
    unsigned int    x, y;

    new = init_matrix(to_x, to_y);

    for (int i = 0; i < to_x; i++){
        for(int j = 0; j < to_y; j++){

            x = (int) roundf((float)i / (float)to_x * (float)old->x);
            y = (int) roundf((float)j / (float)to_y * (float)old->y);

            x = min(x, old->x-1);
            y = min(y, old->y-1);

            value = get_value(old, x, y);

            set_value(new, i, j, value);
        }
    }

    return new;
}

int min(int a, int b){

    if (a>b)
        return b;
    return a;
}


void free_matrix(matrix* matrix) {
    free(matrix->values);
    free(matrix);
}



char get_value(matrix* matrix, int x, int y) {
    return matrix->values[x * matrix->y + y];
}



void set_value(matrix* matrix, int x, int y, char value) {
    matrix->values[x * matrix->y + y] = value;
}



void print_matrix(matrix* values)
{
    int size_x = values->x;
    int size_y = values->y;
    printf("\n");
    int i = 0;
    while (i < size_x)
    {
        int j = 0;
        while (j < size_y)
        {
            int line = i * size_y + j;
            if (values->values[line] == 1)
                printf("%d", values->values[line]);
            else
                printf(" ");
            j++;
        }
        printf("\n");
        i++;
    }
    printf("\n");
}
