#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "../../tools/list.h"
#include "../../tools/matrix.h"
#include "../../tools/write.h"


/*
*   This function takes a list of characters
*   and writes each corresponding pixel to
*   the file "char.txt".
*   +0x30 is added so that we can have the ASCII
*   value of 1 or 0.
*   A letter is added at the end of each character to
*   indicate which character is being treated.
*   Used for training only.
*/


void writetofile(list* char_list){

    char    value;
    char    alpha;
    char    numer;
    char    curr = 0; //stops at 35, a -> z,  0 -> 9
    FILE*   fp = NULL;
    node*   ptr = char_list->tail;
    matrix* current;

    fp = fopen("char.txt","w");

    for(;ptr != NULL;ptr = ptr->next){

        current = ptr->val;

        for(int i = 0; i < current->x; i++){
            for(int j = 0; j < current->y; j++){
                value = get_value(current, i, j);
                if (value == 0 || value == 1)
                    value += 0x30;
                fwrite(&value,1,sizeof(value), fp);
            }
        }

        alpha = ' ';
        fwrite(&alpha,1,sizeof(char),fp);

        if (curr < 26){
            alpha = 'a' + curr%26;
            fwrite(&alpha,1,sizeof(char),fp);
        }
        else if (curr > 25 && curr < 36){
            numer = '0' + curr%26;
            fwrite(&numer,1,sizeof(char),fp);
        }

        alpha = ' ';
        fwrite(&alpha,1,sizeof(char),fp);

        curr++;
    }

    fclose(fp);
}
