#include "matrix.h"
#include "random.h"

 matrix* transpose_matrix( matrix* mat) {
    matrix* out = init_matrix(mat->y, mat->x);
    for (int i = 0; i < mat->y; i++)
    {
        for (int j = 0; j < mat->x; j++)
        {
            float value = get_float(mat, j, i);
            set_float(out, i, j, value);
        }
    }
    return out;
}

void fill_float_matrix( matrix* mat, float value) {
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            set_float(mat, i, j, value);
        }
    }
}

void fill_rand_matrix( matrix* mat) {
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            set_float(mat, i, j, rand_normal());
        }
    }
}

void fill_randbt_matrix(matrix* mat, float a, float b) {
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            set_float(mat, i, j, rand_b(a, b));
        }
    }
}

void fill_arr_matrix(matrix* mat, float values[]) {
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            set_float(mat, i, j, values[i * mat->y + j]);
        }
    }
}

void sum(matrix* mat1,  matrix* mat2) {
    for (int i = 0; i < mat1->x; i++)
    {
        for (int j = 0; j < mat1->y; j++)
        {
            float m1 = get_float(mat1, i, j);
            float m2 = get_float(mat2, 0, j);
            set_float(mat1, i, j, m1+m2);
        }
    }
}

 matrix* sub( matrix* mat1,  matrix* mat2) {
    matrix* out = init_matrix(mat1->x, mat1->y);
    for (int i = 0; i < mat2->x; i++)
    {
        for (int j = 0; j < mat1->y; j++)
        {
            float m1 = get_float(mat1, i, j);
            float m2 = get_float(mat2, i, j);
            set_float(out, i, j, m1-m2);
        }
    }
    return out;
}

 matrix* product(matrix* mat1,  matrix* mat2) {
     matrix* out = init_matrix(mat1->x, mat1->y);
    for (int i = 0; i < mat2->x; i++)
    {
        for (int j = 0; j < mat1->y; j++)
        {
            float m1 = get_float(mat1, i, j);
            float m2 = get_float(mat2, 0, j);
            set_float(out, i, j, m1*m2);
        }
    }
    return out;
}

matrix* half_square(matrix* mat) {
    matrix* out = init_matrix(mat->x, mat->y);
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            float m = get_float(mat, i, j);
            set_float(out, i, j, 0.5*(m*m));
        }
    }
    return out;
}

 matrix* scal_product( matrix* mat, float value) {
     matrix* out = init_matrix(mat->x, mat->y);
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            float m = get_float(mat, i, j) * value;
            set_float(out, i, j, m);
        }
    }
    return out;
}

 matrix* s(matrix* mat) {
    matrix* out = init_matrix(1, 1);
    float m = 0;
    for (int i = 0; i < mat->x; i++)
    {
        for (int j = 0; j < mat->y; j++)
        {
            m += get_float(mat, i, j);
        }
    }
    set_float(out, 0, 0, m);
    return out;
}

 matrix* dot(matrix* mat1, matrix* mat2)
{ 
     matrix* out = init_matrix(mat1->x, mat2->y);

    for (int i = 0; i < mat1->x; i++) 
    {
        for (int j = 0; j < mat2->y; j++) 
        {
            float add = 0;

            for (int k = 0; k < mat1->y; k++) {
                add += get_float(mat1, i, k) *  get_float(mat2, k, j);
            }
            set_float(out, i, j, add);
        } 
    } 

    return out;
}