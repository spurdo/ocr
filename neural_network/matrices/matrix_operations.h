#ifndef MATRIX_OPERATIONS_H
    #define MATRIX_OPERATIONS_H

    void sum( matrix* mat1,  matrix* mat2);
     matrix* product( matrix* mat1,  matrix* mat2);
     matrix* half_square(matrix* mat);
     matrix* scal_product( matrix* mat1, float value);
     matrix* sub( matrix* mat1,  matrix* mat2);
     matrix* s( matrix* mat);

     matrix* transpose_matrix( matrix* mat);

     matrix* dot( matrix* a,  matrix* b);

    void fill_float_matrix( matrix* mat, float value);
    void fill_arr_matrix( matrix* mat, float values[]);
    void fill_rand_matrix( matrix* mat);
    void fill_randbt_matrix(matrix* mat, float a, float b);
#endif