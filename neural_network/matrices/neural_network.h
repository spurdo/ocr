#ifndef NEURAL_NETWORK_H
    #define NEURAL_NETWORK_H
    float sigmoid(float x);
    float sigmoid_derivate(float x);
    matrix* sigmoid_mat( matrix* mat);
    matrix* sigmoid_derivate_mat( matrix* mat);
    matrix* forward_propagation(matrix* input, matrix* hidden_weights, matrix* hidden_bias);
    void backward_propagation(matrix* desired_output, matrix* predicted_output, matrix* output_weights, matrix* hidden_layer_output, matrix* output_bias, matrix* hidden_weights, matrix* input, matrix* hidden_bias, float lr);
    matrix* backprop_d_predicted_output(matrix* desired_output, matrix* predicted_output);
    matrix* backprop_d_hidden_layer(matrix* output_weights, matrix* d_predicted_output, matrix* hidden_layer_output);
    void update_weights(matrix* hidden_layer_output, matrix* d_predicted_output, matrix* output_weights, float lr);
    void update_bias(matrix* d_predicted_output, matrix* output_bias, float lr);
#endif