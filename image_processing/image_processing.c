#include <err.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "../tools/pixel_operations.h"

// TODO: Insert all the above functions.
void init_sdl(){
    // Init only the video part.
    // If it fails, die with an error message.
    if(SDL_Init(SDL_INIT_VIDEO) == 1)
        errx(1,"Could not initialize SDL: %s.\n", SDL_GetError());
}

SDL_Surface* load_image(char *path)
{
    SDL_Surface *img;

    // Load an image using SDL_image with format detection
    // If it fails, die with an error message.
    img = IMG_Load(path);
    if (!img)
        errx(3, "can't load %s: %s", path, IMG_GetError());

    return img;
}

SDL_Surface* display_image(SDL_Surface *img)
{
    SDL_Surface *screen;

    // Set the window to the same size as the image
    screen = SDL_SetVideoMode(img->w, img->h, 0, SDL_SWSURFACE|SDL_ANYFORMAT);
    if (screen == NULL)
    {
        // error management
        errx(1, "Couldn't set %dx%d video mode: %s\n",
                img->w, img->h, SDL_GetError());
    }

    // Blit onto the screen surface
    if(SDL_BlitSurface(img, NULL, screen, NULL) < 0)
        warnx("BlitSurface error: %s\n", SDL_GetError());

    // Update the screen
    SDL_UpdateRect(screen, 0, 0, img->w, img->h);

    // return the screen for further uses
    return screen;
}

void wait_for_keypressed()
{
    SDL_Event event;

    // Wait for a key to be down.
    do
    {
        SDL_PollEvent(&event);
    } while(event.type != SDL_KEYDOWN);

    // Wait for a key to be up.
    do
    {
        SDL_PollEvent(&event);
    } while(event.type != SDL_KEYUP);
}

void grayscale(SDL_Surface* image, int x, int y)
{
    Uint32 pixel = get_pixel(image, x, y);
    Uint8 r, g, b;

    SDL_GetRGB(pixel, image->format, &r, &g, &b);
    float average = 0.3*r + 0.59*g + 0.11*b;

    pixel = SDL_MapRGB(image->format, average, average, average);
    put_pixel(image,x,y,pixel);
}

void convolution(SDL_Surface* input,SDL_Surface* output, double kernel[],size_t kernelLength, int width, int height,int x, int y){
    Uint32 pixel = get_pixel(input, x, y);
    Uint8 r, g, b;
    SDL_GetRGB(pixel, input->format, &r, &g, &b);
    float newR = 0;
    float newG = 0;
    float newB = 0;

    if (x > 0 && x < width - 1 && y > 0 && y < height - 1){
        Uint32 pixels[] = {
            get_pixel(input,x-1,y-1),get_pixel(input,x,y-1),get_pixel(input,x+1,y-1),
            get_pixel(input,x-1,y  ),get_pixel(input,x,y  ),get_pixel(input,x+1,y  ),
            get_pixel(input,x-1,y+1),get_pixel(input,x,y+1),get_pixel(input,x+1,y+1)
        };


        for(size_t i = 0; i < kernelLength; i++){
            Uint8 r,g,b;
            SDL_GetRGB(pixels[i], input->format, &r,&g,&b);
            newR += kernel[i] * r;
            newG += kernel[i] * g;
            newB += kernel[i] * b;
        }

        if (newR > 255) newR = 255;
        if (newR < 0) newR = 0;
        if (newG > 255) newG = 255;
        if (newG < 0) newG = 0;
        if (newB > 255) newB = 255;
        if (newB < 0) newB = 0;
        pixel = SDL_MapRGB(output->format, newR, newG, newB);
        put_pixel(output,x,y,pixel);
    }
}

double get_average_lum_all(SDL_Surface* image, int width, int height){
    double average = 0;
    unsigned int count = 0;
    Uint32 z = 0;
    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){
            Uint32 pixel = get_pixel(image, x, y);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, image->format, &r, &g, &b);
            count += ((r+g+b)/3);
            z+=1;
        }
    }
    average = count/z;
    return average;
}

double get_average_lum_grey(SDL_Surface* image, int width, int height, Uint8 averageAll){ //DONNE LA LUMINOSITE MOYENNE DE LIMAGE
    double average = 0;
    unsigned int count = 0;
    Uint32 z = 0;
    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){
            Uint32 pixel = get_pixel(image, x, y);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, image->format, &r, &g, &b);
            if ((r+g+b)/3 < averageAll){
                count += ((r+g+b)/3);
                z+=1;
            }
        }
    }
    average = count/z;
    return average;
}

void binarization(SDL_Surface* image,int x, int y, int averageLum){

    Uint32      pixel = get_pixel(image, x, y);
    Uint8       r, g, b;

    SDL_GetRGB(pixel, image->format, &r, &g, &b);
    Uint8 averageRGB = (r+g+b)/3;

    if (averageRGB > averageLum){

        r = 255;
        g = 255;
        b = 255;
    }
    else{
        r = 0;
        g = 0;
        b = 0;
    }
    pixel = SDL_MapRGB(image->format, r, g, b);
    put_pixel(image,x,y,pixel);
}

void save_image(SDL_Surface* image_surface){

    SDL_SaveBMP(image_surface, "output.bmp");
}

void black_white(SDL_Surface* image_surface){


    Uint32  pixel;
    Uint8   r, g, b, average;

    int     height, width, i, j;

    height = image_surface->h;
    width = image_surface->w;

    for(j = 0; j < height; j++){
        for(i = 0; i < width; i++){

            pixel = get_pixel(image_surface, i, j);
            SDL_GetRGB(pixel, image_surface->format, &r, &g, &b);

            average = 0.3 * r + 0.59 * g + 0.11 * b;

            if (average > 200){
                average = 255;
            }
            else{
                average = 0;
            }

            r = average;
            g = average;
            b = average;

            pixel = SDL_MapRGB(image_surface->format, r, g, b);

            put_pixel(image_surface, i, j, pixel);
        }
    }
}
