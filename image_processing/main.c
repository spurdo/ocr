#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "../tools/image_processing.h"
#include "../tools/detection.h"
#include "../tools/pixel_operations.h"
#include "../tools/list.h"
#include "../tools/matrix.h"
#include "../tools/write.h"

int main(int argc, char* argv[]){


    if (argc < 2)
        errx(1,"Please input a valid file name");


    double gaussian_blur[9] = {
        0.0625,0.125,0.0625,
        0.125,0.25,0.125,
        0.0625,0.125,0.0625
    };

    int             nblines = 0;
    int             nbcharacters = 0;

    size_t          kernelSize = 9;

    list*           l;
    Tuple           topleft, bottomright;

    SDL_Surface*    image_input;
    SDL_Surface*    image_output;

    SDL_Surface*    screen_surface;

    // Initialize the SDL
    init_sdl();

    image_input = load_image(argv[1]);
    image_output = load_image(argv[1]);

    int             width = image_input->w;
    int             height = image_input->h;

    // Display the image.
    screen_surface = display_image(image_input);

    // Wait for a key to be pressed.
    wait_for_keypressed();

/*
*   =======================
*   Image pre processing
*   =======================
*/

    int averageLum = get_average_lum_all(image_input,width,height);
    averageLum = get_average_lum_grey(image_input,width,height,averageLum);

    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){

            convolution(image_input,image_output,gaussian_blur,kernelSize,width,height,x,y);
            binarization(image_output,x,y,averageLum);
        }
    }

/*
*   ============
*   detection
*   ============
*/


    l = init_list();

    topleft.x = 0;
    topleft.y = 0;

    bottomright.x = image_output->w;
    bottomright.y = image_output->h;

    // Detects sentence from topleft and botright coords
    detectSentence(&topleft, &bottomright,image_output, &nblines, &nbcharacters, l);


    //update_surface(screen_surface,image_input); //test averagelum etc
    SDL_FreeSurface(image_input);

    lprint(l);

    printf("\n\tLines: %d  Characters: %d\n",nblines,nbcharacters);

    update_surface(screen_surface,image_output); //normal one

    if (*argv[2] == '0')
        writetofile(l);

    else if(*argv[2] == '1')
        write(l);

    else
        errx(42,"Invalid parameter.\n0 -> training set\n1 -> recognition set");

    delete_list(l);

    wait_for_keypressed();

    // Save the image
    save_image(image_output);

    // Free the image surface.
    SDL_FreeSurface(image_output);

    // Free the screen surface.
    SDL_FreeSurface(screen_surface);

    return 0;
}
