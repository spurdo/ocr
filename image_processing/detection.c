#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include "SDL/SDL_image.h"
#include "SDL/SDL.h"
#include "../tools/pixel_operations.h"
#include "../tools/detection.h"
#include "../tools/matrix.h"
#include "../tools/list.h"

#define BLACK 0
#define WHITE 255


/*
*   This function takes a character on the image,
*   and proceeds to save it as a matrix.
*   The matrix is then passed on to the interpolation
*   function to have a new matrix of 16x16.
*   The final 16x16 matrix is then appended to a list,
*   containing a matrix with every single character
*   in the image.
*/

void char_to_mat(list* character_list, SDL_Surface* image, int top_x, int top_y, int bot_x, int bot_y){

    matrix* m;
    matrix* new;
    Uint32  c, b;

    b = SDL_MapRGB(image->format, 0, 0, 0);
    m = init_matrix(bot_y-top_y,bot_x-top_x);

    for(int i = top_x; i < bot_x; i++){
        for(int j = top_y; j < bot_y; j++){

            c = get_pixel(image, i, j);

            if (c == b)
                set_value(m, j-top_y, i-top_x, 1);
            else
                set_value(m, j-top_y, i-top_x, 0);
        }
    }
    new = matrix_interpolation(m, 16, 16);
    append(character_list, new);
}

void detectCharacter(Tuple* topleft, Tuple* bottomright, SDL_Surface* image, int* nbcharacters, list* character_list){

    int     white_occ;
    int     top_x, top_y;
    int     bot_x, bot_y;
    int     height;


    int     characters;
    int     foundcharacter;

    Tuple   topchar, botchar;

    Uint32  black,white;
    Uint32  color;

    foundcharacter = 0;
    characters = 0;

    black = SDL_MapRGB(image->format, 0, 0, 0);
    white = SDL_MapRGB(image->format, 255, 255, 255);

    white_occ = 0;
    height = bottomright->y;

    top_x = bottomright->x;
    top_y = bottomright->y;
    bot_x = topleft->x;
    bot_y = topleft->y;

    for (int i = topleft->x; i < image->w; i++){

        for (int j = topleft->y; j < bottomright->y; j++){

            if (foundcharacter == 1 && j == height-1 && white_occ == height - topleft->y-1){
                topchar.x = top_x;
                topchar.y = top_y;
                botchar.x = bot_x+4;
                botchar.y = bot_y+4;

                char_to_mat(character_list,image,top_x,top_y,bot_x,bot_y);

                drawRect(image, &topchar, &botchar);

                top_x = bottomright->x;
                top_y = bottomright->y;
                bot_x = topleft->x;
                bot_y = topleft->y;

                foundcharacter = 0;
                characters++;
            }
        else{
                color = get_pixel(image, i, j);

                if (color == white)
                    white_occ++;

                else if (color == black){
                    foundcharacter = 1;
                    if (i<top_x)
                        top_x = i;
                    if (i>bot_x)
                        bot_x = i;
                    if (j<top_y)
                        top_y = j;
                    if (j>bot_y)
                        bot_y = j;
                }
            }
        }

        white_occ = 0;
    }
    *nbcharacters += characters;
}


void detectSentence(Tuple* topleft, Tuple* bottomright, SDL_Surface* image, int* nblines, int* nbcharacters, list* l){

    int     white_occ;
    int     top_x, top_y;
    int     bot_x, bot_y;
    int     width;

    int     lines;
    int     foundline;

    Tuple   topsentence, botsentence;  //Tuple values of the rectangle to be drawn.

    Uint32  black, white;
    Uint32  color;



    foundline = 0;     //Boolean value to check if we've found a line, in order to draw rect.
    lines = 0;         //Number of lines in the image.

    black = SDL_MapRGB(image->format, 0, 0, 0);
    white = SDL_MapRGB(image->format, 255, 255, 255);

    white_occ = 0;          //Number of occurences of white pixels on one line
    width = bottomright->x;

    top_x = bottomright->x; // These values are here to take the
    top_y = bottomright->y; // left/topmost and right/bottommost
    bot_x = topleft->x;     // pixels of the current sentence we're treating
    bot_y = topleft->y;     // in order to pass them on into the drawRect function


    for(int y = topleft->y; y<image->h; y++){  // We treat all the image from the first black pixel at the top, to the bottom of the image

        for(int x = topleft->x; x<width; x++){ // We treat all pixels between this interval

            if (foundline == 1 && x == width-1 && white_occ == width-topleft->x-1){ // If we've arrived at a blank line
                                                                                    // and we've treated black pixels before
                topsentence.x = top_x;                                              // then we call drawRect on the coordinats
                topsentence.y = top_y;
                botsentence.x = bot_x+1;
                botsentence.y = bot_y+1;

                detectCharacter(&topsentence, &botsentence, image, nbcharacters, l);   // We detect all the characters on this line
                drawRect(image, &topsentence, &botsentence);

                top_x = bottomright->x;   // We reset our comparison vallues
                top_y = bottomright->y;   // for the next sentence (if there is one)
                bot_x = topleft->x;
                bot_y = topleft->y;

                foundline = 0;    // We reset our boolean
                lines++;              // and we add 1 to the number of lines seen
            }
            else{
                color = get_pixel(image, x, y);  // Color of current pixel

                if (color == white)  // If we found a white pixel, we add it to white occurences
                    white_occ++;

                else if (color == black){ // If not, we set leftmost and rightmost values for the sentence
                    foundline = 1;
                    if (x<top_x)
                        top_x = x;
                    if (x>bot_x)
                        bot_x = x;
                    if (y<top_y)
                        top_y = y;
                    if (y>bot_y)
                        bot_y = y;
                }
            }
        }

        white_occ = 0; // We reset white pixel occurences at each new line we treat
    }

    *nblines = lines; // We pass on the number of sentences encountered into the pointer's value
}

void drawRect(SDL_Surface* image, Tuple* topleft, Tuple* bottomright){

    Uint32 pixel = SDL_MapRGB(image->format, 255, 0 ,0);
    int height, width;

    height = bottomright->y;
    width = bottomright->x;

    for (int j  = topleft->y; j<height; j++){
        for (int i = topleft->x; i<width; i++){

            if (j == topleft->y || j == height-1)
                put_pixel(image, i,j, pixel);
            else{
                if (i == topleft->x || i == width-1)
                    put_pixel(image, i, j, pixel);
            }
        }
    }
}


