#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
//#include "Networkstruct.h"
#include "../tools/list.h"
#include "../tools/matrix.h"
#include "../tools/write.h"


/*
*   This function takes a list of characters
*   and writes each corresponding pixel to
*   the file "char.txt".
*   +0x30 is added so that we can have the ASCII
*   value of 1 or 0.
*   A letter is added at the end of each character to
*   indicate which character is being treated.
*   Used for training only.
*/


void writetofile(list* char_list){

    char    maj = 0;
    char    value;
    char    alpha;
    char    numer;
    char    curr = 0; //stops at 61, a -> z, A -> Z,  0 -> 9
    FILE*   fp = NULL;
    node*   ptr = char_list->tail;
    matrix* current;

    fp = fopen("../neural_network/Loic/char.txt","w");

    for(;ptr != NULL;ptr = ptr->next){

        current = ptr->val;

        for(int i = 0; i < current->x; i++){
            for(int j = 0; j < current->y; j++){
                value = get_value(current, i, j);
                if (value == 0 || value == 1)
                    value += 0x30;
                fwrite(&value,1,sizeof(value), fp);
            }
        }

        alpha = ' ';
        fwrite(&alpha,1,sizeof(char),fp);

        if (curr < 26){
            alpha = 'a' + curr%26;
            fwrite(&alpha,1,sizeof(char),fp);
        }

        else if (curr >= 26 && curr < 52){
            alpha = 'A' + maj;
            fwrite(&alpha,1,sizeof(char),fp);
            maj++;
        }
        else if (curr >= 52 && curr < 62){
            numer = '0' + curr%26;
            fwrite(&numer,1,sizeof(char),fp);
        }
        alpha = ' ';
        fwrite(&alpha,1,sizeof(char),fp);

        curr++;
    }

    fclose(fp);
}

void write(list* char_list){

    char    value;

    FILE*   fp = NULL;
    node*   ptr = char_list->tail;
    matrix* current;

    fp = fopen("../neural_network/Loic/char.txt","w");

    for(;ptr != NULL;ptr = ptr->next){

        current = ptr->val;

        for(int i = 0; i < current->x; i++){
            for(int j = 0; j < current->y; j++){
                value = get_value(current, i, j);
                if (value == 0 || value == 1)
                    value += 0x30;
                fwrite(&value,1,sizeof(value), fp);
            }
        }
    }

    fclose(fp);
}


void savebias(hidden_layer* hidden, output_layer* output){

    FILE*   fp;
    int     hidden_len;
    int     output_len;
    char    space = ' ';
    char    newln = '\n';
    float   val;

    fp = fopen("datasave.txt","w");
    hidden_len = hidden->nb_hidden_neurons;
    output_len = output->nb_output_neurons;

    for (int i = 0; i < hidden_len; i++){
        for (int j = 0; j < hidden->neurons[i].nb_hidden_weights; j++){
            val = hidden->neurons[i].hidden_weights[j];
            fprintf(fp,"%f",val);
            fprintf(fp,"%c",space);
        }
        val = hidden->neurons[i].hidden_bias;
        fprintf(fp,"%f",val);
        fprintf(fp,"%c",space);
    }

    fprintf(fp,"%c",newln); //newline seperator for hidden and outputs

    for (int i = 0; i < output_len; i++){
        for (int j = 0; j < output->neurons[i].nb_output_weights; j++){
            val = output->neurons[i].output_weights[j];
            fprintf(fp,"%f",val);
            fprintf(fp,"%c",space);
        }
        val = output->neurons[i].output_bias;
        fprintf(fp,"%f",val);
        fprintf(fp,"%c",space);
    }

    fclose(fp);
}
